const root = document.getElementById('root');

async function findMeByIP() {
    const ipDataPromise = await fetch('https://api.ipify.org/?format=json');
    const ipData = await ipDataPromise.json();

    const ipAddressDataPromise = await fetch('http://ip-api.com/json/'
        + ipData.ip
        + '?fields=status,message,continent,country,countryCode,regionName,city,district,query');
    let ipAddressData = await ipAddressDataPromise.json();

    const p = document.createElement('p');
    //континент, країна, регіон, місто, район.
    const data = [
        'континент - ' + ipAddressData.continent,
        'країна - ' + ipAddressData.country,
        'регіон - ' + ipAddressData.regionName,
        'місто - ' + ipAddressData.city,
        'район - ' + ipAddressData.district,
        'IP - ' + ipData.ip,
    ];
    p.innerHTML = data.join('<br>');

    root.append(p);
    return ipAddressData;
}

window.addEventListener('load', (e) => {
    document.getElementById('but')?.addEventListener('click', (e) => {
        e.preventDefault();
        findMeByIP();
    });
});
